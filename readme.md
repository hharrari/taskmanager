# Projet test Harrari Hakime

## Envirnonnement de développement

### Pré-requis
* PHP 8
* Composer
* Symfony CLI
* Docker
* Docker-compose

Pour vérifier les pré-requis ( hors service Docker ) vous pouvez taper la commande suivante de la CLI Symfony :

```bash
symfony check:requirements
```
### Lancer l'environnement de développement

```bash
docker-compose up -d
symfony serve -d
```

### Premier lancement : alimenter la base de donnée

```bash
composer install
symfony console d:m:m
symfony console d:f:l --no-interaction
```

#### Jeux de données

1 Rôles existent :
* Admin

Pour accéder à l'espace
Login : admin@admin.com
mot de pass : password

