<?php
namespace App\Core\Task\Ports;

use App\Core\Task\Models\Employee;

interface EmployeeRepositoryInterface
{
	public function find(int $id): ?Employee;
    public function findAll(): array;
}