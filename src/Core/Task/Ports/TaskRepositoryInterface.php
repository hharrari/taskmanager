<?php
namespace App\Core\Task\Ports;

use App\Core\Task\Models\Task;

interface TaskRepositoryInterface
{
	public function save(Task $task): void;
	public function find(int $id): ?Task;
}