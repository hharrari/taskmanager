<?php
namespace App\Core\Task\Ports;

use App\Core\Task\Models\Assignment;
use App\Core\Task\Models\Employee;

interface AssignmentRepositoryInterface
{
	/**
	 * @param Employee $employee
	 * @return Assignment[]|null
	 */
    public function findByEmployee(Employee $employee): ?array;
    public function save(Assignment $assignment): void;
}