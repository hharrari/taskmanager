<?php

namespace App\Core\Task\UseCases;

use App\Core\Task\Models\Assignment;
use App\Core\Task\Ports\AssignmentRepositoryInterface;
use App\Core\Task\Ports\EmployeeRepositoryInterface;
use App\Core\Task\Ports\TaskRepositoryInterface;
use DateTimeInterface;
use Exception;

class AssignTaskUseCase
{
    private TaskRepositoryInterface $taskRepository;
    private EmployeeRepositoryInterface $employeeRepository;
    private AssignmentRepositoryInterface $assignmentRepository;

    public function __construct(
        TaskRepositoryInterface $taskRepository,
        EmployeeRepositoryInterface $employeeRepository,
        AssignmentRepositoryInterface $assignmentRepository
    ) {
        $this->taskRepository = $taskRepository;
        $this->employeeRepository = $employeeRepository;
        $this->assignmentRepository = $assignmentRepository;
    }

    public function execute(int $taskId, int $employeeId): void
    {
        $task = $this->taskRepository->find($taskId);
        $employee = $this->employeeRepository->find($employeeId);

        if (!$task || !$employee) {
            throw new Exception("L'employé ou la tâche n'existe pas");
        }

        $assignments = $this->assignmentRepository->findByEmployee($employee);
        $totalHours = 0;
        foreach ($assignments as $assignment) {
            $interval = $assignment->getTask()->getEndTime()->diff($assignment->getTask()->getStartTime());
            $totalHours += $interval->h + ($interval->i / 60);
        }

        $taskDuration = $task->getEndTime()->diff($task->getStartTime())->h + ($task->getEndTime()->diff($task->getStartTime())->i / 60);

        if ($totalHours + $taskDuration > 8) {
            throw new Exception("L'employé ne peut pas travailler plus de 8 heures par jour");
        }

        foreach ($assignments as $assignment) {
            if ($task->getStartTime() < $assignment->getTask()->getEndTime() && $task->getEndTime() > $assignment->getTask()->getStartTime()) {
                throw new Exception("L'employé ne peut pas travailler sur deux tâches en même temps");
            }
        }

        $assignment = new Assignment();
        $assignment->setEmployee($employee);
        $assignment->setTask($task);

        $this->assignmentRepository->save($assignment);
    }
}
