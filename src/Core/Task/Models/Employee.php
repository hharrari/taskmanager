<?php
namespace App\Core\Task\Models;

class Employee
{
    private int $id;
    private string $name;

    public function setId(int $id): static
	{
		$this->id = $id;
		return $this;
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function setName(string $name): static
	{
		$this->name = $name;
		return $this;
	}

	public function getName(): string
	{
		return $this->name;
	}
}