<?php

namespace App\Core\Task\Models;

class Task
{
	private int $id;
	private string $libelle;
	private \DateTimeInterface $startTime;
	private \DateTimeInterface $endTime;

	public function setId(int $id): static
	{
		$this->id = $id;
		return $this;
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function setLibelle(string $libelle): static
	{
		$this->libelle = $libelle;
		return $this;
	}

	public function getLibelle(): string
	{
		return $this->libelle;
	}

	public function setStartTime(\DateTimeInterface $startTime): static
	{
		$this->startTime = $startTime;
		return $this;
	}

	public function getStartTime(): \DateTimeInterface
	{
		return $this->startTime;
	}

	public function setEndTime(\DateTimeInterface $endTime): static
	{
		$this->endTime = $endTime;
		return $this;
	}

	public function getEndTime(): \DateTimeInterface
	{
		return $this->endTime;
	}
}
