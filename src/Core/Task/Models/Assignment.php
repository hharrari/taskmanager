<?php

namespace App\Core\Task\Models;

use App\Core\Task\Models\Employee;
use App\Core\Task\Models\Task;

class Assignment
{
	private Employee $employee;
	private Task $task;

	public function setEmployee(Employee $employee): static
	{
		$this->employee = $employee;
		return $this;
	}

	public function getEmployee(): Employee
	{
		return $this->employee;
	}

	public function setTask(Task $task): static
	{
		$this->task = $task;
		return $this;
	}

	public function getTask(): Task
	{
		return $this->task;
	}
}
