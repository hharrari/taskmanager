<?php

namespace App\Controller\Employee;

use App\Repository\EmployeeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/employee')]
class ListingController extends AbstractController
{
    #[Route('/', name: 'app_employee_index', methods: ['GET'])]
    public function index(
        EmployeeRepository $employeeRepository,
        #[MapQueryParameter] ?int $page = 1
    ): Response {
        return $this->render('employee/index.html.twig', [
            'employees' => $employeeRepository->findPaginated($page),
        ]);
    }
}
