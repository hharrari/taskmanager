<?php

namespace App\Controller\Task;

use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/task')]
class ListingController extends AbstractController
{
    #[Route('/', name: 'app_task_index', methods: ['GET'])]
    public function index(
        TaskRepository $taskRepository,
        #[MapQueryParameter] ?int $page = 1
    ): Response {

        return $this->render('task/index.html.twig', [
            'tasks' => $taskRepository->findPaginated($page),
        ]);
    }
}
