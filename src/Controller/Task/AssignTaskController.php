<?php

namespace App\Controller\Task;

use App\Core\Task\UseCases\AssignTaskUseCase;
use App\Entity\Task;
use App\Form\AssignTaskType;
use App\Form\TaskType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/task')]
class AssignTaskController extends AbstractController
{

    public function __construct(private AssignTaskUseCase $assignTaskUseCase)
    {
        $this->assignTaskUseCase = $assignTaskUseCase;
    }

    #[Route('/{id}/assign', name: 'app_task_assign', methods: ['GET', 'POST'])]
    public function new(Request $request, Task $task): Response
    {
        $form = $this->createForm(AssignTaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $taskId = $form->getData()->getId();
            $employeeId = $form->get('employee')->getData()->getId();
            try {
                $this->assignTaskUseCase->execute($taskId, $employeeId);
            } catch (\Exception $e) {
                $this->addFlash('warning', $e->getMessage());
                return $this->redirectToRoute('app_task_index', [], Response::HTTP_SEE_OTHER);
            }

            $this->addFlash('success', 'La tâche a été assignée avec succès');
            return $this->redirectToRoute('app_task_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('task/assign.html.twig', [
            'task' => $task,
            'form' => $form,
        ]);
    }
}
