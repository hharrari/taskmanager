<?php
namespace App\Controller;

use App\Repository\EmployeeRepository;
use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
	#[Route('/', name: 'app_home_index', methods: ['GET'])]
	public function index(TaskRepository $taskRepository, EmployeeRepository $employeeRepository): Response
	{
		$countTasks = $taskRepository->count([]);
		$countEmployees = $employeeRepository->count([]);

		return $this->render('home.html.twig', [
			'countTasks' => $countTasks,
			'countEmployees' => $countEmployees,
		]);
	}
}
