<?php
namespace App\Infrastructure\Task\Driven;

use App\Core\Task\Models\Task;
use App\Core\Task\Ports\TaskRepositoryInterface;
use App\Entity\Task as TaskEntity;
use App\Repository\TaskRepository;

class SqlTaskRepository implements TaskRepositoryInterface
{
	public function __construct(private TaskRepository $taskRepository)
	{}
	public function save(Task $task): void
	{
		$taskEntity = new TaskEntity();
		$taskEntity->setTitle($task->getLibelle());
		$taskEntity->setStartTime($task->getStartTime());
		$taskEntity->setEndTime($task->getEndTime());

		$this->taskRepository->save($taskEntity);
	}

	public function find(int $id): ?Task
	{
		/** @var TaskEntity $taskEntity */
		$taskEntity = $this->taskRepository->find($id);
		if ($taskEntity === null) {
			return null;
		}

		$task = (new Task())
			->setId($taskEntity->getId())
			->setLibelle($taskEntity->getTitle())
			->setStartTime($taskEntity->getStartTime())
			->setEndTime($taskEntity->getEndTime());

		return $task;

	}
}