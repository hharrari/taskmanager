<?php
namespace App\Infrastructure\Assignment\Driven;

use App\Core\Task\Models\Assignment;
use App\Core\Task\Models\Employee;
use App\Core\Task\Models\Task;
use App\Core\Task\Ports\AssignmentRepositoryInterface;
use App\Entity\Employee as EmployeeEntity;
use App\Entity\Task as TaskEntity;
use Doctrine\ORM\EntityManagerInterface;

class SqlAssignmentRepository implements AssignmentRepositoryInterface
{
	public function __construct(private EntityManagerInterface $entityManager)
	{}
	public function findByEmployee(Employee $employee): array
	{
		$employeeEntity = $this->entityManager->getReference(EmployeeEntity::class, $employee->getId());
		$taskEntities = $employeeEntity->getTask();
		$assignments = [];
		foreach ($taskEntities as $taskEntity) {
			$assignment = (new Assignment())
				->setEmployee($employee)
				->setTask((new Task())
					->setId($taskEntity->getId())
					->setLibelle($taskEntity->getTitle())
					->setStartTime($taskEntity->getStartTime())
					->setEndTime($taskEntity->getEndTime())
				);
			$assignments[] = $assignment;
		}
		return $assignments;
	}

	public function save(Assignment $assignment): void
	{
		$taskEntity = $this->entityManager->getReference(TaskEntity::class, $assignment->getTask()->getId());
		$employeeEntity = $this->entityManager->getReference(EmployeeEntity::class, $assignment->getEmployee()->getId());

		$taskEntity->setEmployee($employeeEntity);

		$this->entityManager->persist($taskEntity);
		$this->entityManager->flush();
	}
}