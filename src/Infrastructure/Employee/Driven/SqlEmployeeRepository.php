<?php
namespace App\Infrastructure\Employee\Driven;

use App\Core\Task\Models\Employee;
use App\Core\Task\Ports\EmployeeRepositoryInterface;
use App\Entity\Employee as EmployeeEntity;
use App\Repository\EmployeeRepository;

class SqlEmployeeRepository implements EmployeeRepositoryInterface
{

	public function __construct(private EmployeeRepository $employeeRepository)
	{}

	public function find(int $id): ?Employee
	{
		/** @var EmployeeEntity $employeeEntity */
		$employeeEntity = $this->employeeRepository->find($id);

		if ($employeeEntity === null) {
			return null;
		}

		$employee = (new Employee())
			->setId($employeeEntity->getId())
			->setName($employeeEntity->getName());

		return $employee;
	}

	public function findAll(): array
	{
		$ListEmployee = $this->employeeRepository->findAll();
		$ListEmployeeModel = [];
		foreach ($ListEmployee as $employeeEntity) {
			$employee = (new Employee())
				->setId($employeeEntity->getId())
				->setName($employeeEntity->getName());
			$ListEmployeeModel[] = $employee;
		}
		return $ListEmployeeModel;
	}
}