<?php

namespace App\DataFixtures;

use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TasksFixtures extends Fixture
{
    public const NUMBER_TASK = 30;

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $day = 0;
        for ($i = 0; $i < self::NUMBER_TASK; $i++) {
            $task = new Task();
            $task->setTitle($faker->sentence(6, true));

            if ($i % 10 == 0 && $i != 0) {
                $day++;
            }

            $startHour = rand(1, 12);
            $endHour = rand($startHour + 1, 12);
            $startMin = rand(0, 59);
            $endMin = rand(0, 59);
            $startTime = (new \DateTimeImmutable())->modify("+$day days +$startHour hours " . $startMin . ' minutes');
            $endTime = (new \DateTimeImmutable())->modify("+$day days +$endHour hours " . $endMin . ' minutes');

            $task->setStartTime($startTime);
            $task->setEndTime($endTime);
            $manager->persist($task);
        }

        $manager->flush();
    }
}