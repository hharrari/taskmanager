<?php

namespace App\DataFixtures;

use App\Entity\Employee;
use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class EmployeesFixtures extends Fixture
{
    public const NUMBER_EPLOYEE = 10;

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $day = 0;
        for ($i = 0; $i < self::NUMBER_EPLOYEE; $i++) {
            $task = new Employee();
            $task->setName($faker->name());
            $manager->persist($task);
        }

        $manager->flush();
    }
}